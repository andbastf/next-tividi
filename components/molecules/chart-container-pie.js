import React, { Component } from 'react';
import { render } from 'react-dom';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';


export default class ChartContainerPie extends Component {


    constructor(props) {


        super(props);

        const data = this.getCurrentInverse();
        this.state = {
            chartOptions:{
                chart: {
                    height: '100%',
                    backgroundColor: 'transparent',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    margin: [2, 0, 5, 0],
                    spacingTop: 0,
                    spacingBottom: 0,
                    spacingLeft: 0,
                    spacingRight: 0,
                    animation: {duration: 1000}
                },
                exporting: {enabled: false}, credits: {style: {fontSize: '0'}}, title: {text: ''},
                legend: {enabled: false},
                tooltip: {enabled: false},
                plotOptions: {
                    pie: {dataLabels: {enabled: false}, borderWidth: 0}
                },
                series: [{
                    data: [{
                        y: data[0]
                    }, {
                        y: data[1],
                        color: 'transparent'
                    }],
                    type: 'pie',
                }]
            }
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevState.chartOptions.series &&
            prevState.chartOptions.series[0].data[0].y !== prevProps.value){
            this.updateSeries();
        }
    }

    getCurrentInverse = () => {
        const val = Math.round(this.props.value);
        return [val, (100 - val)];
    };

    updateSeries = () => {
        const data = this.getCurrentInverse();
        this.setState({
            chartOptions: {
                series: [
                    { data:[
                            {y:data[0],
                                color: 'var(--' + this.props.color + ')'},
                            {y:data[1]}
                        ]}
                ]
            }
        });
    };


    render(){
        return(
            <>
                <style jsx>{`
                  .pieContainer{
                    position: absolute;
                    width: 85%;
                    top: -5.5vh;
                  }
                `}
                </style>
                <div className={'pieContainer'}>
                    <HighchartsReact
                        highcharts={Highcharts}
                        options={this.state.chartOptions}
                    />
                </div>
            </>
        )
    }
}

