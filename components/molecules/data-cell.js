import utilStyles from '../../styles/utils.module.css'
import styles from './data-cell.module.css'
import cn from 'classnames';

export default function DataCell({info, isBig}) {

    let divId = 'dataCell_' + info.id;

    let cellClasses = [
        styles.dataCell,
        {
        [utilStyles.opacity__5] : info.isPast || info.id === 'projection',
    }];

    let valueClasses = [
        utilStyles.number__big,
        {
        'color-white': !info.paintedValue,
        [utilStyles.text__shadowed] : !(info.isPast || info.isFuture),
        [utilStyles.number__mid] : (info.isPast || info.isFuture),
        [utilStyles.opacity__7] : info.id === 'accomplish'
    }];

    let suffixClasses = [
        [styles.dataSuffix],
        [utilStyles.opacity__5],
        [utilStyles.number__small],
        [utilStyles.font__light],
        {
            [utilStyles.opacity__7] : (info.paintedValue || info.isFuture)
    }];


    return (
        <div id={divId} className={cn(cellClasses)}>

            {(info.isPast || (info.isMoney ? info.id === 'current' : true)) &&
                <>
                    { info.isMoney && <div className={styles.dataCurrency}>US$</div>}

                    <span className={cn(valueClasses)}>
                        {info.value}
                        {info.isPercentage && <span className={cn(utilStyles.number__mid,styles.numberPercent)}>%</span>}
                    </span>

                    {(!info.isPast && info.type !== 'pie') &&
                        <span className={cn(suffixClasses)}>
                            <b className={utilStyles.number__mid}>{info.suffixValue}</b>
                            {info.dailySpeed ? <>
                                    <span> C/Día </span>
                                    {info.suffixValueMini && <span className={styles.suffixMini}>
                                    {!isBig && <>= <b>{info.suffixValueMini}</b><small>%</small></>}

                                </span>}
                                </> :
                                <span>{info.isFuture && '%'}</span>
                            }

                        </span>
                    }

                </>
            }

        </div>
    );
}
