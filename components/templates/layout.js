import React from 'react';
import useSWR from 'swr'
import Head from 'next/head'
import styles from './layout.module.css'
import utilStyles from '../../styles/utils.module.css'
import Header from "./header";

import { useRouter } from 'next/router';


export const rootTitle = 'Resultados';
export let dateInfo = {};

const fetcher = url => fetch(url).then(r => r.json());

let k = 0;
let lastRoute = '';

export default function Layout({ children, headTitle, onDataReady }) {


    if(lastRoute !== useRouter().pathname){
        lastRoute = useRouter().pathname;
        k = 0;
    }

    if(k < 5) {
        k++;


        const d = new Date();
        let today = {
            day: d.toLocaleDateString('es-ES', {weekday: 'long'}),
            date: d.getDate(),
            month: d.toLocaleDateString('es-ES', {month: 'long'})
        };

        const lastDateThisMonth = new Date(d.getFullYear(), d.getMonth() + 1, 0).getDate();
        let daysUntilMonthEnd = 0;
        let workingDaysSinceMonthBegin = 0;
        let workingDaysThisMonth = 0;

        for (let i = 1; i <= lastDateThisMonth; i++) {
            const testDay = new Date(d.getFullYear(), d.getMonth(), i).getDay();
            if (testDay !== 0 && testDay !== 6) {
                workingDaysThisMonth++;
                if (i < today.date) {
                    workingDaysSinceMonthBegin++;
                } else {
                    daysUntilMonthEnd++;
                }
            }
        }

        dateInfo = {
            today: today,
            daysUntilMonthEnd: daysUntilMonthEnd,
            workingDaysSinceMonthBegin: workingDaysSinceMonthBegin,
            workingDaysThisMonth: workingDaysThisMonth,
            lastDateThisMonth: lastDateThisMonth
        };


        const dataGroup = {
            objetivos: null,
            historicos: null,
            financials: null
        };


        Object.keys(dataGroup).map((key, i) => {

            const googleSheetsApi = 'https://spreadsheets.google.com/feeds/cells/1I-BnPktl3aUxpOnBRwJmCmAGsxpegoGNpQgWXorRkrw/' + (i + 1) + '/public/full?alt=json';
            const {data, error} = useSWR(googleSheetsApi, fetcher);
            if (data) {
                const formatted = {titles: [], series: []};

                const entry = data.feed.entry;

                entry.forEach(cellData => {

                    const cell = cellData.gs$cell;

                    const idRow = parseInt(cell.row, 0) - 2;
                    const idCol = parseInt(cell.col, 0) - 2;

                    if (idRow < 0) {
                        formatted.titles.push(cell.$t);
                    } else {

                        if (idCol < 0) {
                            formatted.series.push({
                                id: cell.$t,
                                values: [],
                                numericValues: []
                            });
                        } else if (formatted.series[idRow]) {
                            if (cell.numericValue) {
                                formatted.series[idRow].numericValues.push(parseFloat(cell.numericValue));
                            }
                            formatted.series[idRow].values.push(cell.$t);
                        }
                    }

                });

                dataGroup[key] = formatted;

            }

        });
        onDataReady({dateInfo:dateInfo, data:dataGroup});
    }


    return (
        <div className={styles.container}>
            <Head>
                <link rel="icon" href="/favicon.ico" />
                <title>{headTitle} | Tividi</title>
                <meta
                    name="description"
                    content="Learn how to build a personal website using Next.js"
                />
                <meta name="og:title" content={rootTitle} />
                <meta name="twitter:card" content="summary_large_image" />
            </Head>
            <main>
                <Header dateInfo={dateInfo}/>
                <div className={styles.container}>
                    {children}
                </div>
            </main>
        </div>
    )


}
