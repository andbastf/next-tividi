import React from 'react';
import Today from "../organisms/today";
import Navigation from "../organisms/navigation";
import utils from "../../styles/utils.module.css";
import styles from "./header.module.css";
import cn from 'classnames';

export default function Header({dateInfo}) {

    return (
        <div className={cn(styles.header, utils.flex, utils.flexGrow)}>
            <div>
                <img className={styles.logo} src={'/images/logos/logo-white.png'} />
            </div>
            <div className={utils.flexGrow}>
                <Navigation/>
            </div>
            <style jsx>{`
              .today-container{
                width:40%;
                text-align: right;
              }`}
            </style>
            <div className={'today-container'}>
                <Today dateInfo={dateInfo}/>
            </div>
        </div>
    )
};
