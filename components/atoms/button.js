import Link from "next/link"
import utils from "../../styles/utils.module.css";

export default function Button({data, activeClassName}) {
    return(
        <Link href={data.href}>
            <span className={utils.title}>{data.title}</span>
        </Link>
    )
}
