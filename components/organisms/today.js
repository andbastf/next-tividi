import {Component} from "react";
import utils from '../../styles/utils.module.css'
import cn from 'classnames'

export default function Today({dateInfo}){

    const today = dateInfo.today;
    if(today){
        return (
            <div className={cn(utils.number__mid,utils.font__light)}>
                <span className={utils.text__capitalize}>{today.day}</span>
                <b> {today.date} </b>
                de
                <span className={utils.text__capitalize}> {today.month}.</span>
                <br />
                <span className={utils.opacity__3}>
            (Van <b>{dateInfo.workingDaysSinceMonthBegin}</b> días hábiles)
        </span>
                <br />
                Cierre en <b>{ dateInfo.daysUntilMonthEnd }</b> días hábiles.
            </div>

        );
    }else{

        return null;
    }




}
