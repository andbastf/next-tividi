import cn from 'classnames'
import utils from '../../styles/utils.module.css'

export default function CurrentMonth({isBig}) {

    let d = new Date();
    let currentMonth = d.toLocaleDateString('es-ES', {month: 'long'});
    return (
        <>
            <style jsx>{`
              .monthContainer{
                border: .2vw solid white;
                width: 100%;
                height: 100%;
                border-radius: 2vh;
              }
              .month{
                padding: 1vh 2vh;
                background: var(--color-black);
                display: initial;
                left: 1vw;
                position: relative;
                top: -2vh;
              }
              .futureBackground{
                height: 100%;
                width: 12%;
                background: var(--color-grey);
                float: right;
                position: absolute;
                right: 0;
                z-index: -1;
                top: 0;
                border-radius: 0 2vh 2vh 0;
              }
            `}</style>
            <div className={cn('monthContainer',utils.textAlign__left)}>
                <div className={cn('month',utils.title,utils.text__capitalize)}>
                    {currentMonth}
                </div>
                <div className={'futureBackground'}> </div>
            </div>
        </>
    )
}
