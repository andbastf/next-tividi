import React from "react";
import DataCell from "../molecules/data-cell";
import styles from "./serie-result.module.css"
import utils from '../../styles/utils.module.css'
import cn from 'classnames'
import ChartContainerPie from "../molecules/chart-container-pie";

export default function SerieResult({serie, isBig}) {

    const rowInfo = [
        {id: 'past1', type: 'past', isPast: true},
        {id: 'past2', type: 'past', isPast: true},
        {id: 'past3', type: 'past', isPast: true },
        {type: 'divider'},
        {id: 'current', type: 'big', title: 'Actual', dailySpeed: true},
        {id: 'objective', type: 'big', title: 'Objetivo', paintedValue: true, dailySpeed: true},
        {id: 'accomplish', type: 'pie', title: <>Cumplimiento<br />del Objetivo</>, smallTitle: true, isPercentage:true},
        {id: 'projection', type: 'big', title: <>Proyectadas<br />Cuentas / Día<br />Actuales</>, paintedValue: true, smallTitle: true, isPercentage:true },
        {id: 'future', type: 'future', title: 'Por Caer', smallTitle: true, isFuture: true}
    ];

    let color = 'color-' + serie.id;

    return (
        <>
            <div className={ cn(styles.serieContainer, utils.flex, utils.flexAlignCenter, { [styles.boldBorder] : serie.id === 'total'}) }>
                <div className={ cn(utils.flex, utils.flexColumn, styles.fx_title) }>
                    <div className={styles.fx_titleAux}> </div>
                    <div className={cn(styles.fx_titleContent,color,utils.title,utils.textAlign__right)}>
                        {serie.title}
                        {(serie.id === 'aum') &&
                            <div className={cn(utils.font__light,utils.number__mid)}><small>x US$ 1,000</small></div>
                        }
                    </div>
                </div>
                {
                    rowInfo.map((row, index) => {


                            let rowStyles = [
                                utils.position__relative,
                                styles['fx_' + row.type],
                                {
                                    [color] : row.paintedValue
                                }];
                            let needsSmall = (row.isPast || row.smallTitle);
                            let titleStyle = [
                                styles.cellTitle,
                                utils.font__light,
                                {
                                    [styles['title-' + row.id]] : true,
                                    [utils.number__small] : needsSmall,
                                    [utils.number__mid] : !needsSmall,
                                    [utils.opacity__3] : (row.id === 'projection')
                                }];

                            let shownValues = serie.shownValues[row.id];
                            let cellInfo = {};
                            if(shownValues){
                                cellInfo = {
                                    ...row,
                                    value: shownValues ? shownValues.value : 43,
                                    suffixValue: shownValues ? shownValues.suffix : 41,
                                    suffixValueMini: shownValues ? shownValues.mini : 40,
                                    isMoney: serie.isMoney,
                                };
                            }

                            const isProjection = row.id !== 'projection';
                            if(isBig || isProjection) {
                                return (
                                    <div key={index} className={cn(rowStyles)}>
                                        {serie.hasTitles &&
                                        <div className={cn(titleStyle)}>{row.title}</div>
                                        }
                                        {row.type !== 'divider' && <>
                                            <DataCell info={cellInfo} isBig={isBig}> </DataCell>
                                            {!serie.isMoney && row.id === 'accomplish' &&
                                            <ChartContainerPie value={cellInfo.value} color={color}/>}
                                        </>
                                        }
                                    </div>
                                );
                            }


                    })
                }
                {!isBig && <div className={styles.fx_big}> </div> }
            </div>
        </>
    )
}
