import React from "react";
import Button from "../atoms/button";
import utils from "../../styles/utils.module.css";
import styles from "./navigation.module.css";
import { useRouter } from 'next/router';
import cn from 'classnames';
import Link from "next/link";

export default function Navigation() {
    const routes= [
        { href: '/', title: 'Resultados' },
        { href: '/finanzas', title: 'Finanzas' },
    ];

    return (
        <div className={utils.flex}>
            {
                routes.map(route=>{
                    return(
                        <Link href={route.href}>
                            <div  key={route.toString()}  className={cn(styles.tab,
                                {[styles.active]: (useRouter().pathname === route.href) })} >
                                <span className={utils.title}>{route.title}</span>
                            </div>
                        </Link>
                    )
                })
            }
        </div>
    )
}
