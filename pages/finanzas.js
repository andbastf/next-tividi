import Layout  from '../components/templates/layout'
import React, {Component} from 'react';

export default class Finanzas extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    dataReady = (data) =>{
        console.log(data);
    }

    render(){
        return (
            <Layout headTitle={'Financials'} onDataReady={this.dataReady}>
                Página de Finanzas
            </Layout>
        )
    }
}
