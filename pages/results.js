import React from 'react';
import Layout from '../components/templates/layout';
import {Component} from "react";
import CurrentMonth from "../components/organisms/current-month";
import SerieResult from "../components/organisms/serie-result";
import styles from "./results.module.css";
import cn from 'classnames';

export default class Results extends Component{

    constructor(props){
        super(props);
        this.state = {
            isBig : false,
            series: [
                {
                    id: 'auto', title: 'Automática', hasTitles: true, shownValues: this.getShownValues()
                },
                {
                    id: 'trade', title: 'Trading', shownValues: this.getShownValues()
                },
                {
                    id: 'total', title: 'TOTAL', shownValues: this.getShownValues()
                },
                {
                    id: 'aum', title: 'A.U.M.', isMoney: true, shownValues: this.getShownValues()
                }
            ],
            hideAll:true
        };
    }

    getShownValues(){
        return {
            past1: {value: 10},
            past2: {value: 11},
            past3: {value: 12},
            current: {value: 1, suffix: 2, mini: 3},
            objective: {value: 4, suffix: 5},
            accomplish: {value:13},
            projection: {value: 6, suffix: 7},
            future: {value: 8, suffix: 9},
        };
    }

    toggleBig = () => {
        this.setState({isBig: !this.state.isBig});
    };

    dataReady = (dataReady) => {



        const fechas = dataReady.dateInfo;
        if(dataReady.data) {

            const dataHistoricos = dataReady.data.historicos;
            const dataObjetivos = dataReady.data.objetivos;

            if (dataHistoricos && dataObjetivos) {
                const series = [];
                dataHistoricos.series.forEach((serie, i) => {
                    const dataObjetivoSerie = dataObjetivos.series[i];
                    const values = dataObjetivoSerie.values;
                    series.push({
                        pastData: serie.numericValues,
                        id: dataObjetivoSerie.id,
                        currentData: parseFloat(values[0]),
                        objectiveData: parseFloat(values[1]),
                        futureData: parseFloat(values[2]),
                    });
                });

                let seriesToUpdate = this.state.series;
                series.forEach((serie, i) => {
                    const thisSerie = this.state.series.find(serie => serie.id === serie.id);
                    if (thisSerie) {

                        const needsDecimals = thisSerie.isMoney;
                        const thisSerieValues = {};
                        const currentValue = serie.currentData;
                        const objectiveValue = serie.objectiveData;
                        const futureValue = serie.futureData;

                        const currentSuffixValue = this.customRound(
                            (serie.currentData /
                                ((fechas.workingDaysSinceMonthBegin > 0) ? fechas.workingDaysSinceMonthBegin : 1)
                            ), true);

                        const projectionSuffix = this.customRound(
                            (currentSuffixValue * fechas.workingDaysThisMonth), needsDecimals);
                        const projectionValue = this.customRound(objectiveValue ? (projectionSuffix * 100) / objectiveValue : 0, false);


                        if (serie.pastData) {
                            thisSerieValues.past1 = {value: serie.pastData[0]};
                            thisSerieValues.past2 = {value: serie.pastData[1]};
                            thisSerieValues.past3 = {value: serie.pastData[2]};
                            const orders = [serie.pastData[0], serie.pastData[1], serie.pastData[2], currentValue, objectiveValue];


                            const ordered = orders.concat().sort((a, b) => {
                                return a - b;
                            });
                            const indexes = [];
                            orders.forEach(val => {
                                indexes.push(ordered.concat().findIndex(e => e === val));
                            });

                            thisSerie.lineOrders = indexes;

                        }


                        thisSerieValues.current = {
                            value: this.customRound(currentValue, needsDecimals),
                            suffix: currentSuffixValue,
                            mini: projectionValue
                        };

                        thisSerieValues.objective = {
                            value: objectiveValue,
                            suffix: this.customRound((objectiveValue / fechas.workingDaysThisMonth), true)
                        };

                        thisSerieValues.projection = {
                            value: projectionValue,
                            suffix: projectionSuffix
                        };

                        thisSerieValues.future = {
                            value: futureValue,
                            suffix: this.customRound(objectiveValue ? (((currentValue + futureValue) * 100) / objectiveValue) : 0, needsDecimals)
                        };

                        thisSerieValues.accomplish = {
                            value: this.customRound((objectiveValue > 0) ? ((currentValue * 100) / objectiveValue) : 0)
                        };
                        seriesToUpdate[i] = {...seriesToUpdate[i], shownValues: thisSerieValues};

                    }


                });


                this.setState({'series': seriesToUpdate, 'hideAll': false});

            }

        }
    };


    customRound = (value, needsDecimals) => {
        const rounder = needsDecimals ? 10 : 1;
        return Math.round(value * rounder) / rounder;
    };

    render(){

        return (
            <Layout headTitle={'Resultados'} onDataReady={this.dataReady}>

                <div style={{opacity: this.state.hideAll ? 0 : 1 }}>
                    <div className={cn(styles.currentMonthContainer,{[styles.currentMonthContainerOnBig]: this.state.isBig})}>
                        <CurrentMonth isBig={this.state.isBig}/>
                    </div>
                    <div className={styles.seriesContainer}>
                        {this.state.series.map(thisSerie => {
                            return (
                            <SerieResult key={thisSerie.id} serie={thisSerie} isBig={this.state.isBig}>
                            </SerieResult>)
                        })}
                    </div>

                    <div className={styles.sizer} onClick={this.toggleBig}>
                        { this.state.isBig ? '<' : '>'}
                    </div>
                </div>

            </Layout>
        )
    }

};
